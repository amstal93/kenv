#[macro_use]
extern crate lazy_static;

pub mod arx;
pub mod cmd;
pub mod paths;
pub mod prelude;
pub mod tools;
pub mod tui;

use prelude::*;
use tools::docker::Docker;

fn main() -> AppResult<()> {
  install_tools_if_missing()?;
  let mut exit_code = 0;
  if let Err(err) = run() {
    Tui::fail(err.to_string());
    exit_code = 1;
  };
  std::process::exit(exit_code);
}

fn install_tools_if_missing() -> AppResult<()> {
  let mut tools_missing = false;
  for tool in tools::all() {
    let path = tool.get_path()?;
    if !path.exists() {
      tools_missing = true;
      break;
    }
  }
  if tools_missing {
    cmd::update_tools(false)?;
  }
  if Docker::list_images().is_err() {
    return err(ErrorKind::Other, "docker engine is not available");
  }
  Ok(())
}

fn run() -> AppResult<()> {
  let commands = &cmd::all_commands();
  let matches = app(commands).get_matches();
  for command in commands.iter() {
    if let Some(result) = command.run(&matches) {
      return result;
    }
  }
  app(commands).print_long_help().unwrap();
  Ok(())
}

fn app<'a>(commands: &[Box<dyn cmd::Command>]) -> clap::App<'a, 'a> {
  let mut app = clap::App::new(PKG_NAME)
    .version(PKG_VERSION)
    .author(PKG_AUTHORS)
    .long_about(PKG_DESCRIPTION)
    .settings(CMD_SETTINGS.as_slice());
  for cmd in commands.iter() {
    let cmd = cmd.spec().settings(CMD_SETTINGS.as_slice());
    app = app.subcommand(cmd);
  }
  app
}
