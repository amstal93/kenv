pub mod docker;
pub mod helm;
pub mod k9s;
pub mod kind;
pub mod kubectl;
pub mod tigera_operator;

use crate::prelude::*;
use docker::*;
use helm::Helm;
use k9s::K9s;
use kind::Kind;
pub use kubectl::{Context, Kubectl};
use std::path::PathBuf;
use std::process::Command;
use std::str::FromStr;

pub fn all() -> Vec<Box<dyn Tool>> {
  vec![
    Box::new(Docker {}),
    Box::new(Helm {}),
    Box::new(Kind {}),
    Box::new(K9s {}),
    Box::new(Kubectl {}),
  ]
}

pub trait Tool {
  fn can_download(&self) -> bool;
  fn get_name(&self) -> &'static str;
  fn get_latest_version_url(&self) -> &'static str;
  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)>;
  fn get_version(&self) -> AppResult<String>;

  fn get_os_name(&self) -> String {
    let mut name = self.get_name().to_owned();
    if std::env::consts::OS == "windows" {
      name = format!("{}.exe", &name);
    }
    name
  }

  fn get_local_path(&self) -> AppResult<PathBuf> {
    let name = self.get_os_name();
    Ok(paths::mkdir_bin()?.join(&name))
  }

  fn get_path(&self) -> AppResult<PathBuf> {
    // first, check if local tool is available (managed by kenv)
    let local_path = self.get_local_path()?;
    if local_path.exists() {
      return Ok(local_path);
    }
    // second, check if system tool is available (managed by OS)
    let name = self.get_os_name();
    let system_path = std::env::var("PATH")?;
    let delim = match std::env::consts::OS {
      "windows" => ';',
      _ => ':',
    };
    for path in system_path.split(delim) {
      let path = PathBuf::from_str(path)?.join(&name);
      if path.exists() {
        return Ok(path);
      }
    }
    // if tool is not found, return local path
    Ok(local_path)
  }

  fn get_latest_version(&self) -> AppResult<String> {
    let latest_version_url = self.get_latest_version_url();
    let output = curl(latest_version_url, None)?.text()?;
    if output.contains("href") {
      let url = output.split('"').nth(1).unwrap_or("").to_owned();
      let version = url.split('/').last().unwrap_or("").to_owned();
      let version = version
        .strip_prefix('v')
        .map(|x| x.to_owned())
        .unwrap_or(version);
      Ok(version)
    } else {
      let version = output;
      let version = version
        .strip_prefix('v')
        .map(|x| x.to_owned())
        .unwrap_or(version);
      Ok(version)
    }
  }

  /// 1. Downloads supplied version of an application to ~/.local/share/kuber/bin/downloads/<tool>/<version>/<filename>
  /// 2. (in case necessary) Extracts files into ~/.local/share/kuber/bin/<name>
  fn download(&self, version: &str) -> AppResult<PathBuf> {
    if !self.can_download() {
      return err(ErrorKind::Other, "cannot download");
    }
    let di = self.get_download_info();
    let (url_format, archive_path) = match di {
      Some((x, y)) => (x, y),
      None => panic!(
        "{} is not supported on {} OS",
        PKG_NAME,
        std::env::consts::OS
      ),
    };
    let url = url_format.to_owned().replace("{}", version);
    let filename = url.split('/').last().unwrap().to_owned();
    let src = paths::mkdir_downloads(self.get_name(), version)?.join(&filename);
    let is_new = !src.exists();
    if is_new {
      let mut response = curl(url.as_str(), Some(10))?;
      let status = response.status();
      if !status.is_success() {
        return err(ErrorKind::Other, "cannot download file");
      }
      let mut file = match std::fs::File::create(&src) {
        Ok(f) => f,
        Err(_) => return err(ErrorKind::Other, "cannot create file"),
      };
      std::io::copy(&mut response, &mut file)?;
    }
    let target_path = self.get_local_path()?;
    if target_path.exists() && is_new {
      std::fs::remove_file(&target_path)?;
    }
    if filename.ends_with(".tar.gz") || filename.ends_with(".tgz") {
      arx::untargz(&src, &target_path)?;
    } else if filename.ends_with(".zip") {
      let root = archive_path.unwrap_or("").to_owned();
      arx::unzip(&src, &target_path, root)?;
    } else {
      std::fs::copy(&src, &target_path)?;
    }
    #[cfg(not(windows))]
    {
      use std::os::unix::fs::PermissionsExt;
      std::fs::set_permissions(&target_path, PermissionsExt::from_mode(0o755))?;
    }
    Ok(target_path)
  }

  fn mk_command(&self) -> AppResult<Command> {
    let path = self.get_path()?;
    Ok(std::process::Command::new(path))
  }
}
