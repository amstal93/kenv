use crate::prelude::*;

pub fn update(dry_run: bool) -> AppResult<()> {
  Tui::chapter("Updating tools");
  let mut tools = tools::all();
  let mut errors = vec![];
  for tool in &mut tools {
    let path = tool.get_path()?;
    let path = path.to_str().unwrap_or("<unknown_path>");
    Tui::write(Text::ok(">>", path, "|"), None);
    // Tui::write(Text::ok(">>", tool.get_name(), "|"), None);
    if !tool.can_download() {
      if let Ok(version) = tool.get_version() {
        Tui::grayln(version);
      } else {
        Tui::redln("not installed");
        errors.push(format!("cannot find {} in the system", tool.get_name()));
      }
      continue;
    }
    let latest = tool.get_latest_version().unwrap_or_default();
    if let Ok(version) = tool.get_version() {
      if version != latest {
        Tui::yellow(version);
        Tui::suffix(">>");
        if !dry_run {
          Tui::yellow(&latest);
          Tui::suffix("...");
          match tool.download(latest.as_str()) {
            Ok(_) => Tui::greenln("done"),
            Err(error) => Tui::redln(format!("{}", &error)),
          }
        } else {
          Tui::yellowln(&latest);
        }
      } else {
        Tui::green("up-to-date");
        Tui::suffix("<<");
        Tui::greenln(&version);
      }
    } else if latest.is_empty() {
      Tui::redln("failed to locate latest version");
    } else {
      Tui::suffix("...");
      Tui::suffix(">>");
      if !dry_run {
        Tui::yellow(&latest);
        Tui::suffix("...");
        match tool.download(latest.as_str()) {
          Ok(_) => Tui::greenln("done"),
          Err(error) => Tui::redln(format!("{}", &error)),
        }
      } else {
        Tui::yellowln(&latest);
      }
    }
  }
  Tui::divider();
  if !errors.is_empty() {
    let msg = errors.join("\n");
    return err(ErrorKind::Other, msg);
  }
  Ok(())
}
