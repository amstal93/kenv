use crate::{
  prelude::*,
  tools::{helm::Helm, kind::Kind, Kubectl},
};

#[derive(Debug)]
pub struct AppsListArgs {
  pub cluster_name: String,
  pub namespace: Option<String>,
}

pub fn list(args: &AppsListArgs) -> AppResult<()> {
  Tui::chapter(format!(
    "Listing applications in [{}] cluster",
    &args.cluster_name
  ));
  let context = Kind::cluster_name_to_context(args.cluster_name.as_str());
  let mut namespaces = Kubectl::list_namespaces(&context)?;
  namespaces.sort();
  for ns in namespaces {
    if let Some(namespace) = &args.namespace {
      if &ns != namespace {
        continue;
      }
    }
    Tui::writeln(
      Text::ok(">>", ns.as_str(), "|"),
      Some(StateText::Warn("...")),
    );
    let mut index = 0;
    let releases =
      Helm::list_releases(args.cluster_name.as_str(), ns.as_str())?;
    for release in releases {
      index += 1;
      let prefix = format!("{msg:>width$}", msg = index.to_string(), width = 2);
      Tui::write(Text::log(prefix.as_str(), release.name.as_str(), "|"), None);
      if release.status == "deployed" {
        Tui::green(release.status);
      } else {
        Tui::red(release.status);
      }
      Tui::gray(" (rev: ");
      let msg = format!("{}", release.revision);
      Tui::yellow(&msg);
      Tui::grayln(")");
    }
  }
  Tui::divider();
  Ok(())
}
