use crate::prelude::*;
use crate::tools::docker::{Container, Docker};
use crate::tools::kind::Kind;
use crate::tools::{Context, Kubectl};

pub fn list() -> AppResult<()> {
  Tui::chapter("Running clusters");
  let clusters = list_clusters()?;
  for cluster in &clusters {
    let mut state = None;
    if cluster.is_current {
      state = Some(StateText::Warn("( ---------> current <--------- )"));
    }
    let title = format!(
      "{} (masters: {}, workers: {})",
      cluster.name, cluster.masters, cluster.workers
    );
    Tui::writeln(Text::ok(">>", title.as_str(), "|"), state);
    // version
    Tui::writeln(
      Text::ok("   -", "version:", "|"),
      Some(StateText::Ok(cluster.version.as_str())),
    );
    // mounts
    if !cluster.mounts.is_empty() {
      Tui::writeln(Text::ok("   -", "mounts:", "|"), None);
      for (host_path, container_path) in cluster.mounts.iter() {
        Tui::writeln(
          Text::log("     -", host_path.as_str(), "|"),
          Some(StateText::Log(container_path.as_str())),
        );
      }
    }
    // ports
    if !cluster.ports.is_empty() {
      Tui::writeln(Text::ok("   -", "ports:", "|"), None);
      for (host_port, container_port) in cluster.ports.iter() {
        Tui::writeln(
          Text::log("     -", host_port.as_str(), "|"),
          Some(StateText::Log(container_port.as_str())),
        );
      }
    }
    Tui::divider();
  }
  Ok(())
}

#[derive(Debug)]
struct Cluster {
  pub name: String,
  pub version: String,
  pub ports: Vec<(String, String)>,
  pub masters: usize,
  pub workers: usize,
  pub context: String,
  pub is_current: bool,
  pub mounts: Vec<(String, String)>,
}

fn list_clusters() -> AppResult<Vec<Cluster>> {
  let names = Kind::list_clusters()?;
  let contexts = Kubectl::list_contexts()?;
  let containers = Docker::list_running_containers()?;
  let mut result = vec![];
  for name in names {
    let master_prefix = format!("{}-control-plane", &name);
    let masters = containers
      .iter()
      .filter(|x| x.name.contains(master_prefix.as_str()))
      .collect::<Vec<&Container>>();
    if masters.is_empty() {
      continue;
    }
    let worker_prefix = format!("{}-worker", &name);
    let workers = containers
      .iter()
      .filter(|x| x.name.contains(worker_prefix.as_str()));
    let master = masters[0];
    let version = master
      .image
      .split(':')
      .last()
      .unwrap_or_default()
      .trim_start_matches('v')
      .to_owned();
    let context_name = format!("kind-{}", &name);
    let cont = contexts
      .iter()
      .filter(|x| *x.name == context_name)
      .collect::<Vec<&Context>>();
    if cont.len() != 1 {
      continue;
    }
    let context = cont[0];
    let cluster = Cluster {
      name,
      version,
      ports: master.ports.to_owned(),
      masters: masters.len(),
      workers: workers.count(),
      context: context.name.to_owned(),
      is_current: context.is_current,
      mounts: master.mounts.clone(),
    };
    result.push(cluster);
  }
  Ok(result)
}
