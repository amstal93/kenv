use crate::{
  prelude::*,
  tools::{docker::Docker, kind::Kind},
};

#[derive(Debug)]
pub struct ClusterStopArgs {
  pub name: String,
  pub registry_name: String,
  pub registry_ui_name: String,
}

impl Default for ClusterStopArgs {
  fn default() -> Self {
    ClusterStopArgs {
      name: "local".to_owned(),
      registry_name: "local-registry".to_owned(),
      registry_ui_name: "local-registry-ui".to_owned(),
    }
  }
}

pub fn down(args: &ClusterStopArgs) -> AppResult<()> {
  Tui::chapter(format!("Shutting down [{}] cluster", &args.name));
  if let Err(error) = stop_cluster(args) {
    Tui::redln(format!("{}", &error));
  }
  if let Err(error) = stop_registry_ui(args) {
    Tui::redln(format!("{}", &error));
  }
  if let Err(error) = stop_registry(args) {
    Tui::redln(format!("{}", &error));
  }
  Tui::divider();
  Ok(())
}

fn stop_cluster(args: &ClusterStopArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Shutting down cluster", "|"), None);
  if !Kind::list_clusters()?.contains(&args.name) {
    Tui::yellowln("already down");
  } else if Kind::stop(&args.name).is_ok() {
    Tui::greenln("done");
  } else {
    Tui::redln("failed");
  }
  Ok(())
}

fn stop_registry(args: &ClusterStopArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Shutting down registry", "|"), None);
  let exists = Docker::list_running_containers()?
    .iter()
    .any(|c| c.name == args.registry_name);
  if !exists {
    Tui::yellowln("already down");
  } else if Docker::stop(&args.registry_name).is_ok() {
    Tui::greenln("done");
  } else {
    Tui::redln("failed");
  }
  Ok(())
}

fn stop_registry_ui(args: &ClusterStopArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Shutting down registry UI", "|"), None);
  let exists = Docker::list_running_containers()?
    .iter()
    .any(|c| c.name == args.registry_ui_name);
  if !exists {
    Tui::yellowln("already down");
  } else if Docker::stop(&args.registry_ui_name).is_ok() {
    Tui::greenln("done");
  } else {
    Tui::redln("failed");
  }
  Ok(())
}
