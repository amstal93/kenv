use crate::prelude::*;

pub struct Docker {}

#[derive(Debug)]
pub struct Image {
  pub id: String,
  pub repository: String,
  pub tag: String,
}

#[derive(Debug)]
pub struct Container {
  pub id: String,
  pub image: String,
  pub ports: Vec<(String, String)>,
  pub name: String,
  pub mounts: Vec<(String, String)>,
}

impl Container {
  pub fn get_control_plane_name(&self) -> Option<String> {
    if !self.name.contains("-control-plane") {
      return None;
    }
    Some(self.name.trim_end_matches("-control-plane").to_owned())
  }
}

impl tools::Tool for Docker {
  fn can_download(&self) -> bool {
    false
  }

  fn get_name(&self) -> &'static str {
    "docker"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://docker.com/"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    None
  }

  fn get_version(&self) -> AppResult<String> {
    let output = self
      .mk_command()?
      .arg("version")
      .arg("--format")
      .arg("{{.Client.Version}}")
      .output()?;
    let output = String::from_utf8(output.stdout)?;
    let version = output.trim_end().to_owned();
    Ok(version)
  }
}

impl Docker {
  pub fn pull<S: AsRef<str>>(image: S) -> AppResult<()> {
    let output = Self::mk_command(&Self {})?
      .arg("pull")
      .arg(image.as_ref().to_owned())
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "cannot pull image");
    }
    Ok(())
  }

  pub fn list_images() -> AppResult<Vec<Image>> {
    let output = Self::mk_command(&Self {})?.arg("images").output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to list images");
    }
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1).collect::<Vec<_>>();
    let mut result = vec![];
    for line in lines {
      let parts = line
        .split("  ")
        .filter(|x| !(*x).is_empty())
        .collect::<Vec<_>>();
      let n = parts.len();
      if n <= 4 {
        continue;
      }
      let image = Image {
        id: parts[2].trim().to_owned(),
        repository: parts[0].trim().to_owned(),
        tag: parts[1].trim().to_owned(),
      };
      result.push(image);
    }
    Ok(result)
  }

  fn fetch_mounts_for_control_plane<S: AsRef<str>>(
    container_name: S,
  ) -> AppResult<Vec<(String, String)>> {
    if !container_name.as_ref().ends_with("-control-plane") {
      return Ok(vec![]);
    }
    let output = Self::mk_command(&Self {})?
      .arg("inspect")
      .arg("-f")
      .arg("{{.Mounts}}")
      .arg(container_name.as_ref())
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to fetch mounts");
    }
    // {bind  /lib/modules /lib/modules  ro false rprivate}
    // {bind  /home/roku/Projects/kenv/_data /data   true rprivate}
    // {bind  /home/roku/Projects/kenv/_logs /logs   true rprivate}
    // {volume b961aee73bcd7efd3e908b11dfac6c1a179b6cb6b7fc992560005cda947c0140 /var/lib/docker/volumes/b961aee73bcd7efd3e908b11dfac6c1a179b6cb6b7fc992560005cda947c0140/_data /var local  true }
    let mounts = String::from_utf8(output.stdout)?
      .trim()
      .trim_start_matches('[')
      .trim_end_matches(']')
      .replace("} {", "}\n{")
      .split('\n')
      .map(|x| x.trim_start_matches('{').trim_end_matches('}').to_owned())
      .filter(|x| x.starts_with("bind") && x.ends_with("true rprivate"))
      .map(|x| {
        let parts = x
          .split_ascii_whitespace()
          .skip(1)
          .take(2)
          .map(|x| x.to_owned())
          .collect::<Vec<String>>();
        (parts[0].to_owned(), parts[1].to_owned())
      })
      .collect::<Vec<(String, String)>>();
    Ok(mounts)
  }

  pub fn list_running_containers() -> AppResult<Vec<Container>> {
    let output = Self::mk_command(&Self {})?.arg("ps").output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to list containers");
    }
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1).collect::<Vec<_>>();
    let mut result = vec![];
    for line in lines {
      let parts = line
        .split("  ")
        .filter(|x| !(*x).is_empty())
        .collect::<Vec<_>>();
      let n = parts.len();
      if n <= 4 {
        continue;
      }
      let ports = parts[n - 2]
        .trim()
        .to_owned()
        .split(", ")
        .filter_map(|x| {
          let parts = x.split("->").collect::<Vec<_>>();
          if parts.len() != 2 {
            None
          } else {
            Some((parts[0].to_owned(), parts[1].to_owned()))
          }
        })
        .collect::<Vec<_>>();
      let name = parts[n - 1].trim().to_owned();
      let mounts = Self::fetch_mounts_for_control_plane(name.as_str())?;
      let container = Container {
        id: parts[0].trim().to_owned(),
        image: parts[1].trim().to_owned(),
        ports,
        name,
        mounts,
      };
      result.push(container);
    }
    Ok(result)
  }

  pub fn is_container_running<S: AsRef<str>>(name: S) -> AppResult<bool> {
    let output = Self::mk_command(&Self {})?
      .arg("inspect")
      .arg("-f")
      .arg("{{.State.Running}}")
      .arg(name.as_ref())
      .output()?;
    if !output.status.success() {
      let err_msg = String::from_utf8(output.stderr)?;
      if err_msg.starts_with("Error: No such object") {
        return Ok(false);
      }
      return err(ErrorKind::Other, err_msg.trim());
    }
    let output = String::from_utf8(output.stdout)?;
    let result = "true" == output.trim();
    Ok(result)
  }

  pub fn run_detached<S: AsRef<str>>(args: Vec<S>) -> AppResult<()> {
    let mut command = Self::mk_command(&Self {})?;
    let mut command = command.arg("run").arg("-d");
    for arg in args {
      command = command.arg(arg.as_ref());
    }
    let output = command.output()?;
    if !output.status.success() {
      let err_msg = String::from_utf8(output.stderr)?;
      return err(ErrorKind::Other, err_msg.trim());
    }
    Ok(())
  }

  pub fn stop<S: AsRef<str>>(name: S) -> AppResult<bool> {
    let output = Self::mk_command(&Self {})?
      .arg("rm")
      .arg("-f")
      .arg(name.as_ref())
      .output()?;
    Ok(output.status.success())
  }

  pub fn try_connect_network_with_container<S: AsRef<str>>(
    network_name: S,
    container_name: S,
  ) -> AppResult<bool> {
    let output = Self::mk_command(&Self {})?
      .args(&[
        "network",
        "connect",
        network_name.as_ref(),
        container_name.as_ref(),
      ])
      .output()?;
    Ok(output.status.success())
  }
}
