use crate::prelude::*;

pub struct TigeraOperator {}

impl tools::Tool for TigeraOperator {
  fn can_download(&self) -> bool {
    false
  }

  fn get_name(&self) -> &'static str {
    "tigera-operator"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://github.com/projectcalico/calico/releases/latest"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    Some(("https://github.com/projectcalico/calico/releases/download/v{}/tigera-operator-v{}-[].tgz", None))
  }

  fn get_version(&self) -> AppResult<String> {
    Ok("unknown".to_owned())
  }
}
