use super::kind::Kind;
use crate::prelude::*;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;
use std::thread::spawn;

pub struct Helm {}

impl tools::Tool for Helm {
  fn can_download(&self) -> bool {
    true
  }

  fn get_name(&self) -> &'static str {
    "helm"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://github.com/helm/helm/releases/latest"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    match std::env::consts::OS {
      "linux" => {
        Some(("https://get.helm.sh/helm-v{}-linux-amd64.tar.gz", None))
      }
      "windows" => Some((
        "https://get.helm.sh/helm-v{}-windows-amd64.zip",
        Some("windows-amd64"),
      )),
      _ => None,
    }
  }

  fn get_version(&self) -> AppResult<String> {
    let output = self.mk_command()?.arg("version").arg("--short").output()?;
    let output = String::from_utf8(output.stdout)?;
    let version = output
      .trim_end()
      .split('+')
      .next()
      .unwrap_or_default()
      .trim_start_matches('v')
      .to_owned();
    Ok(version)
  }
}

#[derive(Debug)]
pub struct HelmRelease {
  pub namespace: String,
  pub name: String,
  pub revision: u16,
  pub status: String,
}

impl Helm {
  pub fn list_releases<S: AsRef<str>>(
    cluster_name: S,
    namespace: S,
  ) -> AppResult<Vec<HelmRelease>> {
    if !Kind::list_clusters()?.contains(&cluster_name.as_ref().to_owned()) {
      return err(ErrorKind::NotFound, "supplied cluster does not exist");
    }
    let context = Kind::cluster_name_to_context(cluster_name);
    let output = Self::mk_command(&Self {})?
      .arg("--kube-context")
      .arg(context.as_str())
      .arg("-n")
      .arg(namespace.as_ref())
      .arg("ls")
      .arg("-a")
      .output()?;
    if !output.status.success() {
      return err(
        ErrorKind::Other,
        "cannot list releases in a given namespace",
      );
    }
    let output = String::from_utf8(output.stdout)?;
    let lines = output.lines().skip(1);
    let mut result = vec![];
    for line in lines {
      let parts = line
        .split_ascii_whitespace()
        .map(|x| x.to_owned())
        .collect::<Vec<String>>();
      let namespace = parts[1].to_owned();
      let name = parts[0].to_owned();
      let revision = parts[2].parse::<u16>().unwrap();
      let status = parts[7].to_owned();
      result.push(HelmRelease {
        namespace,
        name,
        revision,
        status,
      });
    }
    result.sort_by(|a, b| a.name.cmp(&b.name));
    Ok(result)
  }

  pub fn update_dependencies<S: AsRef<str>>(
    cluster_name: S,
    chart_path: PathBuf,
    force: bool,
  ) -> AppResult<bool> {
    if !Kind::list_clusters()?.contains(&cluster_name.as_ref().to_owned()) {
      return err(ErrorKind::NotFound, "supplied cluster does not exist");
    }
    if !chart_path.exists() {
      return err(
        ErrorKind::NotFound,
        "supplied chart location does not exist",
      );
    }
    let tmpcharts_path = chart_path.join("tmpcharts");
    if tmpcharts_path.exists() {
      std::fs::remove_dir_all(tmpcharts_path)?;
    }
    let deps_path = chart_path.join("charts");
    if !deps_path.exists() || force {
      let output = Self::mk_command(&Self {})?
        .current_dir(chart_path)
        .arg("dependency")
        .arg("update")
        .output()?;
      return Ok(output.status.success());
    }
    Ok(true)
  }

  pub fn upgrade_install_wait<S: AsRef<str>>(
    cluster_name: S,
    namespace: Option<S>,
    params: Vec<String>,
    release: S,
    chart_path: S,
    timeout: S,
    verbose: bool,
  ) -> AppResult<()> {
    let context = Kind::cluster_name_to_context(cluster_name);
    let namespace = namespace
      .map(|x| x.as_ref().to_owned())
      .unwrap_or_else(|| "default".to_owned());
    let mut command = Self::mk_command(&Self {})?;
    let mut command = command
      .stdout(std::process::Stdio::piped())
      .stderr(std::process::Stdio::piped())
      .arg("--kube-context")
      .arg(context.as_str())
      .arg("-n")
      .arg(&namespace)
      .arg("upgrade")
      .arg("--install")
      .arg("--create-namespace")
      .arg("--timeout")
      .arg(timeout.as_ref())
      .arg("--wait");
    for param in params {
      command = command.arg("--set").arg(param.as_str());
    }
    command = command.arg(release.as_ref());
    command = command.arg(chart_path.as_ref());
    let mut proc = command.spawn()?;
    let stdout_reader = BufReader::new(proc.stdout.take().unwrap());
    let stderr_reader = BufReader::new(proc.stderr.take().unwrap());
    let error_sink = spawn(move || {
      stderr_reader.lines().for_each(Self::create_logger(verbose))
    });
    stdout_reader.lines().for_each(Self::create_logger(verbose));
    error_sink.join().unwrap();
    let status = proc.wait().unwrap();
    if !status.success() {
      return err(ErrorKind::Other, "failed to install helm release");
    }
    Ok(())
  }

  fn create_logger(verbose: bool) -> fn(Result<String, std::io::Error>) {
    if !verbose {
      return |_| {};
    }
    |line: Result<String, std::io::Error>| {
      let line = line.unwrap_or_default();
      Tui::writeln(Text::log("  ", line.as_str(), ""), None);
    }
  }
}
