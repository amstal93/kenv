use uuid::Uuid;

pub const PKG_NAME: &str = env!("CARGO_PKG_NAME");
pub const PKG_DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");
pub const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
pub const PKG_AUTHORS: &str = env!("CARGO_PKG_AUTHORS");

lazy_static! {
  pub static ref CMD_SETTINGS: Vec<clap::AppSettings> = vec![
    clap::AppSettings::DeriveDisplayOrder,
    clap::AppSettings::ColorAuto,
    clap::AppSettings::ColoredHelp,
  ];
  pub static ref INSTANCE_ID: Uuid = Uuid::new_v4();
  pub static ref SHORT_INSTANCE_ID: String =
    INSTANCE_ID.to_string().split('-').collect::<Vec<_>>()[4].to_owned();
}
